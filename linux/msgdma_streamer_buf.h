/*
 * MSGDMA driver for stream access via Altera mSGDMA IP cores
 *
 * Copyright (C) 2020 Christian Hättich <feddischson@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 */
#ifndef _DMA_CIRC_BUF_H
#define _DMA_CIRC_BUF_H 1

#if !defined MSGDMA_STRM_BUF_N_PAGES
#error MSGDMA_STRM_BUF_N_PAGES must be pre-defined
#endif

#if !defined MSGDMA_STRM_BUF_PAGE_SIZE
#error MSGDMA_STRM_BUF_PAGE_SIZE must be pre-defined
#endif

#if !defined MSGDMA_STRM_BUF_N_ROWS
#error MSGDMA_STRM_BUF_N_ROWS must be pre-defined
#endif

#define MSGDMA_STRM_BUF_ROW_SIZE (MSGDMA_STRM_BUF_N_PAGES * MSGDMA_STRM_BUF_PAGE_SIZE)
#define MSGDMA_STRM_BUF_SIZE (MSGDMA_STRM_BUF_ROW_SIZE * MSGDMA_STRM_BUF_N_ROWS)

/* Circular Buffer for DMA Transfers:
 *  This circular buffer is multi-dimensional.
 *  Lets define that there are MSGDMA_STRM_BUF_N_ROWS rows.
 *  Each row has MSGDMA_STRM_BUF_N_PAGES pages and
 *  each page consists of MSGDMA_STRM_BUF_PAGE_SIZE bytes.
 *
 *  The rows are organized as circular buffer where one row (a scatterlist)
 *  is given to the DMA. This means that no access to this row is possible
 *  during the DMA transfer.
 *
 *  The overall buffer size is
 *  MSGDMA_STRM_BUF_N_ROWS * MSGDMA_STRM_BUF_N_PAGES * DMA_PUF_PAGE_SIZE.
 *
 *  The head and tail is pointing to the rows and head_byte and tail_byte
 *  to specific bytes (relative to the beginning of the overall buffer).
 *
 *  Head and head_byte as well as tail and tail_byte is updated together.
 *
 *  See also Documentation/core-api/circular-buffers.rst
 *
 */
struct msgdma_strm_buf {
  struct scatterlist buf[MSGDMA_STRM_BUF_N_ROWS][MSGDMA_STRM_BUF_N_PAGES];
  struct page *pages[MSGDMA_STRM_BUF_N_ROWS][MSGDMA_STRM_BUF_N_PAGES];
  dma_cookie_t dmac;
  enum dma_transfer_direction trans_dir;
  struct dma_slave_config slave_config;
  struct dma_chan *dma_chan;
  size_t head;
  size_t head_byte;
  size_t tail;
  size_t tail_byte;
  // wait_queue_head_t outq;
  wait_queue_head_t inq;


  /* Is initialized with 0 and set once to 1 after
   * first read or write access to initiate first dma transfer */
  int started;


  /* the producer set the flag to 1 (if full) and
   * the consumer clears it (to continue)*/
  int stopped;

  /* Is set after release to stop any further DMA transfers */
  int do_stop;
};

/* Calculates byte-, page- and row- index of absolute byte.
 */
static inline void dma_buf_calc_idx(size_t byte, size_t *byte_idx,
                                    size_t *page_idx, size_t *row_idx) {
  size_t p = byte / MSGDMA_STRM_BUF_PAGE_SIZE;
  *byte_idx = byte % MSGDMA_STRM_BUF_PAGE_SIZE;
  *row_idx = (p / MSGDMA_STRM_BUF_N_PAGES) % MSGDMA_STRM_BUF_N_ROWS;
  *page_idx = p % MSGDMA_STRM_BUF_N_PAGES;
}

static inline void dma_buf_move_head(struct msgdma_strm_buf *buf) {
  buf->head_byte = (buf->head_byte + MSGDMA_STRM_BUF_ROW_SIZE) % MSGDMA_STRM_BUF_SIZE;
  buf->head = (buf->head + 1) % MSGDMA_STRM_BUF_N_ROWS;
}

static inline void dma_buf_move_tail(struct msgdma_strm_buf *buf) {
  buf->tail_byte = (buf->tail_byte + MSGDMA_STRM_BUF_ROW_SIZE) % MSGDMA_STRM_BUF_SIZE;
  buf->tail = (buf->tail + 1) % MSGDMA_STRM_BUF_N_ROWS;
}

static inline size_t dma_buf_cnt_bytes(struct msgdma_strm_buf *buf) {
  return ((buf->head_byte - buf->tail_byte)) % MSGDMA_STRM_BUF_SIZE;
}

static inline size_t dma_buf_size_bytes(struct msgdma_strm_buf *buf) {
  return ((buf->tail_byte - buf->head_byte - MSGDMA_STRM_BUF_ROW_SIZE)) % MSGDMA_STRM_BUF_SIZE;
}

/*
 * Foreach-loop to iterate over all pages between __start_byte and __end_byte
 * (absolute byte indexes) where __end_byte is the first byte which is not
 * transferred.
 * __s_byte, __s_page and __s_row indicates always the start indexes
 * of the current iterations.
 * __e_byte, __e_page, __e_row stays the same through all iterations
 * and are the end index (corresponds to __end_byte).
 * So __s_byte, __s_page and __s_row is initialized via __start_byte and
 * updated by each iteration.
 * In addition, __len gives length information of the current iteration.
 *
 */
#define foreach_rd_page(__start_byte, __end_byte, __s_byte, __s_page, __s_row, \
                        __e_byte, __e_page, __e_row, __len, __new_row)         \
  for (dma_buf_calc_idx(__start_byte, &__s_byte, &__s_page, &__s_row),         \
       dma_buf_calc_idx(__end_byte, &__e_byte, &__e_page, &__e_row),           \
       __len = (__s_page == __e_page && __s_row == __e_row)                    \
                   ? __e_byte - __s_byte                                       \
                   : MSGDMA_STRM_BUF_PAGE_SIZE - __s_byte,                             \
       __new_row = 1;                                                          \
       __len > 0; __new_row = __s_row,                                         \
       __start_byte = (__start_byte + __len) % MSGDMA_STRM_BUF_SIZE,                   \
       dma_buf_calc_idx(__start_byte, &__s_byte, &__s_page, &__s_row),         \
       __new_row = (__new_row == __s_row) ? 0 : 1,                             \
       __len = (__s_page == __e_page && __s_row == __e_row)                    \
                      ? __e_byte - __s_byte                                    \
                      : MSGDMA_STRM_BUF_PAGE_SIZE - __s_byte)

/*
 * Foreach-loop to iterate over all pages between __start_byte and __end_byte
 * (absolute byte indexes) where __end_byte is the first byte which is not
 * transferred.
 * __s_byte, __s_page and __s_row indicates always the start indexes
 * of the current iterations.
 * __e_byte, __e_page, __e_row stays the same through all iterations
 * and are the end index (corresponds to __end_byte).
 * So __s_byte, __s_page and __s_row is initialized via __start_byte and
 * updated by each iteration.
 * In addition, __len gives length information of the current iteration.
 *
 */
#define foreach_wr_page(__start_byte, __end_byte, __s_byte, __s_page, __s_row, \
                        __e_byte, __e_page, __e_row, __len, __end_row)         \
  for (dma_buf_calc_idx(__start_byte, &__s_byte, &__s_page, &__s_row),         \
       dma_buf_calc_idx(__end_byte, &__e_byte, &__e_page, &__e_row),           \
       __len = (__s_page == __e_page && __s_row == __e_row)                    \
                   ? __e_byte - __s_byte                                       \
                   : MSGDMA_STRM_BUF_PAGE_SIZE - __s_byte,                             \
       __end_row = (((s_page * MSGDMA_STRM_BUF_PAGE_SIZE + __s_byte + __len ) %       \
                        MSGDMA_STRM_BUF_ROW_SIZE ) == 0) ? 1 : 0;                      \
       __len > 0;                                                              \
       __start_byte = (__start_byte + __len) % MSGDMA_STRM_BUF_SIZE,                   \
       dma_buf_calc_idx(__start_byte, &__s_byte, &__s_page, &__s_row),         \
       __len = (__s_page == __e_page && __s_row == __e_row)                    \
                      ? __e_byte - __s_byte                                    \
                      : MSGDMA_STRM_BUF_PAGE_SIZE - __s_byte,                          \
       __end_row = (((s_page * MSGDMA_STRM_BUF_PAGE_SIZE + __s_byte + __len ) %       \
                        MSGDMA_STRM_BUF_ROW_SIZE ) == 0) ? 1 : 0 )

#endif /* _DMA_CIRC_BUF_H */
