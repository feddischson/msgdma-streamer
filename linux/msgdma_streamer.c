/*
 * MSGDMA driver for stream access via Altera mSGDMA IP cores
 *
 * Copyright (C) 2020 Christian Hättich <feddischson@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 */

#include <linux/cdev.h>
#include <linux/circ_buf.h>
#include <linux/delay.h>
#include <linux/dma-mapping.h>
#include <linux/dmaengine.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#define DEV_NAME "msgdma_streamer"

#define MSGDMA_STRM_STREAMER_MAJOR 0

#define MSGDMA_STRM_BUF_N_PAGES 32
#define MSGDMA_STRM_BUF_N_ROWS 64
#define MSGDMA_STRM_BUF_PAGE_SIZE PAGE_SIZE
#include "msgdma_streamer_buf.h"

/* Must be 1, there is one device (and one minor number) per platform device */
#define MSGDMA_STRM_STREAMER_NR_DEVS 1

struct msgdma_strm_dev {
	int minor_cnt;
};

static atomic_t msgdma_strm_available = ATOMIC_INIT(1);

struct msgdma_strm {
	struct platform_device *pdev;
	dev_t dev;
	int major;
	int minor;

	struct cdev cdev; /* Char device structure */

	struct msgdma_strm_buf *rd_buf; /* read buffer */
	struct msgdma_strm_buf *wr_buf; /* write buffer */
};

struct msgdma_strm_dev msgdma_strm_device;
static int msgdma_strm_submit(struct msgdma_strm_buf *buf);

static void dma_callback(void *param)
{
	struct msgdma_strm_buf *buf = (struct msgdma_strm_buf *)param;

	if (buf->trans_dir == DMA_DEV_TO_MEM) {
		/* Check if at least one row is free.
     * As the head is increased always by one row, this is sufficient to avoid
     * collisions
     */
		if (CIRC_SPACE(buf->head, buf->tail, MSGDMA_STRM_BUF_N_ROWS) ==
		    0 || buf->do_stop == 1) {
			buf->stopped = 1;
      printk(KERN_WARNING "MSGDMA transfer stopped\n");
		} else {
			dma_buf_move_head(buf);
			msgdma_strm_submit(buf);
			wake_up_interruptible(&buf->inq);
		}
	} else {
		/* Check if at least one row is available.
     * As the head is increased always by one row, this is sufficient to avoid
     * collisions
     */
		if (CIRC_CNT(buf->head, buf->tail, MSGDMA_STRM_BUF_N_ROWS) ==
		    0 || buf->do_stop == 1) {
			buf->stopped = 1;
      printk(KERN_WARNING "MSGDMA transfer stopped\n");
		} else {
			dma_buf_move_tail(buf);
			msgdma_strm_submit(buf);
			wake_up_interruptible(&buf->inq);
		}
	}
}

static int msgdma_strm_submit(struct msgdma_strm_buf *buf)
{
	struct dma_async_tx_descriptor *desc;

	/* Get a descriptor for transaction */
	if (buf->trans_dir == DMA_DEV_TO_MEM) {
		desc = dmaengine_prep_slave_sg(buf->dma_chan,
					       &buf->buf[buf->head][0],
					       MSGDMA_STRM_BUF_N_PAGES,
					       buf->trans_dir, 0);
	} else if (buf->trans_dir == DMA_MEM_TO_DEV) {
		desc = dmaengine_prep_slave_sg(buf->dma_chan,
					       &buf->buf[buf->tail][0],
					       MSGDMA_STRM_BUF_N_PAGES,
					       buf->trans_dir, 0);
	} else {
		return -1;
	}

	/* Set callback */
	desc->callback = dma_callback;
	desc->callback_param = (void *)buf;

	/* Sumbmit the transaction */
	buf->dmac = dmaengine_submit(desc);

	/* Issue pending DMA requests and wait for callback notification */
	dma_async_issue_pending(buf->dma_chan);

	return 0;
}

static int msgdma_strm_open(struct inode *inode, struct file *filp)
{
	int ret;
	struct msgdma_strm *streamer;

	if (!atomic_dec_and_test(&msgdma_strm_available)) {
		atomic_inc(&msgdma_strm_available);
		ret = -EBUSY; /* already open */
		goto failed1;
	}

	streamer = container_of(inode->i_cdev, struct msgdma_strm, cdev);
	filp->private_data = streamer; /* for other methods */

	/* Allocate a DMA slave channels */
	streamer->rd_buf->dma_chan =
		dma_request_chan(&streamer->pdev->dev, "s2m");
	if (IS_ERR(streamer->rd_buf->dma_chan)) {
		ret = PTR_ERR(streamer->rd_buf->dma_chan);
		goto failed2;
	}

	streamer->wr_buf->dma_chan =
		dma_request_chan(&streamer->pdev->dev, "m2s");
	if (IS_ERR(streamer->wr_buf->dma_chan)) {
		ret = PTR_ERR(streamer->wr_buf->dma_chan);
		goto failed2;
	}

	/* Set slave and controller specific parameters */
	streamer->rd_buf->slave_config.direction =
		streamer->rd_buf->trans_dir; /* transfer direction */
	ret = dmaengine_slave_config(streamer->rd_buf->dma_chan,
				     &streamer->rd_buf->slave_config);
	if (ret < 0) {
		goto failed2;
	}

	streamer->wr_buf->slave_config.direction =
		streamer->wr_buf->trans_dir; /* transfer direction */
	ret = dmaengine_slave_config(streamer->wr_buf->dma_chan,
				     &streamer->wr_buf->slave_config);
	if (ret < 0) {
		goto failed2;
	}

  streamer->rd_buf->started = 0;
  streamer->wr_buf->started = 0;
  streamer->rd_buf->stopped = 0;
  streamer->wr_buf->stopped = 0;
  streamer->rd_buf->do_stop = 0;
  streamer->wr_buf->do_stop = 0;
  streamer->rd_buf->head = 0;
  streamer->wr_buf->head = 0;
  streamer->rd_buf->tail = 0;
  streamer->wr_buf->tail = 0;
  streamer->rd_buf->head_byte = 0;
  streamer->wr_buf->head_byte = 0;
  streamer->rd_buf->tail_byte = 0;
  streamer->wr_buf->tail_byte = 0;

	return 0;

failed2:
	atomic_inc(&msgdma_strm_available); /* release the device */
failed1:
	return ret;
}

static int msgdma_strm_release(struct inode *inode, struct file *filp)
{
	struct msgdma_strm *streamer;
	streamer = (struct msgdma_strm *)filp->private_data;

  /* set do_stop flag 
   * TODO is there any race-condition?
   * */
  streamer->rd_buf->do_stop = 1;
  streamer->wr_buf->do_stop = 1;

	while (1) {
		enum dma_status status =
			dma_async_is_tx_complete(streamer->rd_buf->dma_chan,
						 streamer->rd_buf->dmac, NULL,
						 NULL);
		if (status == DMA_COMPLETE) {
			break;
		} else {
			mdelay(1);
		}
	}

	streamer = container_of(inode->i_cdev, struct msgdma_strm, cdev);
	dma_release_channel(streamer->rd_buf->dma_chan);
	dma_release_channel(streamer->wr_buf->dma_chan);
	atomic_inc(&msgdma_strm_available); /* release the device */
	return 0;
}

static ssize_t msgdma_strm_read(struct file *filp, char __user *ubuf,
				size_t count, loff_t *f_pos)
{
	void *page_addr;
	size_t available;

	size_t start_byte;
	size_t end_byte;

	size_t s_byte;
	size_t s_page;
	size_t s_row;

	size_t e_byte;
	size_t e_page;
	size_t e_row;

	size_t len;
	size_t new_row;

	struct msgdma_strm *streamer;
	struct device *dev;
	struct msgdma_strm_buf *buf;

	size_t ret_cnt = 0;

	streamer = (struct msgdma_strm *)filp->private_data;
	dev = &streamer->pdev->dev;
	buf = streamer->rd_buf;

	available = dma_buf_cnt_bytes(buf);
	while (available <  MSGDMA_STRM_BUF_ROW_SIZE ) {
		/* Do initial DMA transfer */
		if (!buf->started) {
			buf->started = 1;
			msgdma_strm_submit(streamer->rd_buf);
		}

		if (filp->f_flags & O_NONBLOCK) {
			return -EAGAIN;
		}
		if (wait_event_interruptible(
			    buf->inq,
			    (available = dma_buf_cnt_bytes(buf)) >= MSGDMA_STRM_BUF_ROW_SIZE)) {
			return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
		}
	}

	if (count > available) {
		count = available;
	}

	/* calculate start and end byte */
	start_byte = buf->tail_byte % MSGDMA_STRM_BUF_SIZE;
	end_byte = (start_byte + count) % MSGDMA_STRM_BUF_SIZE;

	foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte,
			e_page, e_row, len, new_row)
	{
		/* copy from s_byte to end of this page */
		page_addr =
			page_address(streamer->rd_buf->pages[s_row][s_page]);

		/* Sync the scatter-list when accessing a new row */
		if (new_row) {
			dma_sync_sg_for_cpu(dev,
					    &streamer->rd_buf->buf[s_row][0],
					    MSGDMA_STRM_BUF_N_PAGES,
					    DMA_FROM_DEVICE);
		}

		if (copy_to_user(ubuf + ret_cnt, page_addr + s_byte, len) !=
		    0) {
			/* if there have not been copied any bytes before, return error-code */
			if (ret_cnt == 0) {
				ret_cnt = -EFAULT;
				/* otherwise return the number of bytes copied */
			}
			goto out;
		} else {
			ret_cnt += len;
		}
	}

out:
	/* check if data has been copied. */
	if (ret_cnt >= 0) {
		*f_pos += ret_cnt;

		/* update tail */
		buf->tail_byte =
			(buf->tail_byte + ret_cnt) % MSGDMA_STRM_BUF_SIZE;
		dma_buf_calc_idx(buf->tail_byte, &e_byte, &e_page, &e_row),
			buf->tail = e_row;

		/* initiate next dma transfer if required and if space is available */
		if (buf->stopped && CIRC_SPACE(buf->head, buf->tail,
					       MSGDMA_STRM_BUF_N_ROWS) > 0) {
			buf->stopped = 0;
			dma_buf_move_head(buf);

			/* TODO check return value */
			msgdma_strm_submit(buf);
		}
	}
	return ret_cnt;
}

static ssize_t msgdma_strm_write(struct file *filp, const char __user *ubuf,
				 size_t count, loff_t *f_pos)
{
#if 0
  void *page_addr;
  size_t size;

  size_t start_byte;
  size_t end_byte;

  size_t s_byte;
  size_t s_page;
  size_t s_row;

  size_t e_byte;
  size_t e_page;
  size_t e_row;

  size_t len;
  size_t end_row;

  struct msgdma_strm *streamer;
  struct device *dev;
  struct msgdma_strm_buf *buf;

  size_t ret_cnt = 0;

  streamer = (struct msgdma_strm *)filp->private_data;
  dev = &streamer->pdev->dev;
  buf = streamer->wr_buf;

  size = dma_buf_size_bytes(buf);
  while (size < MSGDMA_STRM_BUF_ROW_SIZE) {
    if (filp->f_flags & O_NONBLOCK) {
      return -EAGAIN;
    }
    if (wait_event_interruptible(
            buf->inq, (size = dma_buf_size_bytes(buf)) >= MSGDMA_STRM_BUF_ROW_SIZE)) {
      return -ERESTARTSYS; /* signal: tell the fs layer to handle it */
    }
  }

  if (count > size) {
    count = size;
  }

  /* calculate start and end byte */
  start_byte = buf->head_byte;
  end_byte = (start_byte + count) % MSGDMA_STRM_BUF_SIZE;

  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                  e_row, len, end_row) {
    /* copy from s_byte to end of this page */
    page_addr = page_address(streamer->rd_buf->pages[s_row][s_page]);

    if (copy_from_user(page_addr + s_byte, ubuf + ret_cnt, len) != 0) {
      /* if there have not been copied any bytes before, return error-code */
      if (ret_cnt == 0) {
        ret_cnt = -EFAULT;
        /* otherwise return the number of bytes copied */
      }
      goto out;
    } else {
      ret_cnt += len;
    }

    /* Sync the scatter-list afterwards finishing a row */
    if (end_row) {
      dma_sync_sg_for_device(dev, &streamer->rd_buf->buf[s_row][0],
                              MSGDMA_STRM_BUF_N_PAGES, DMA_FROM_DEVICE);
    }
  }

out:
  /* check if data has been copied.
   * if yes, update tail */
  if (ret_cnt >= 0) {
    *f_pos += ret_cnt;

    buf->head_byte = (buf->head_byte + ret_cnt) % MSGDMA_STRM_BUF_SIZE;
    dma_buf_calc_idx(buf->head_byte, &e_byte, &e_page, &e_row),
        buf->head = e_row;

    /* initiate next dma transfer if required and if space is available */
    if (!buf->started && CIRC_CNT(buf->head, buf->tail, MSGDMA_STRM_BUF_N_ROWS) > 0) {
      buf->started = 1;
      msgdma_strm_submit(buf);
    } else if (buf->stopped &&
               CIRC_CNT(buf->head, buf->tail, MSGDMA_STRM_BUF_N_ROWS) > 0) {
      buf->stopped = 0;
      dma_buf_move_tail(buf);

      /* TODO check return value */
      msgdma_strm_submit(buf);
    }
  }
  return ret_cnt;
#endif
	return 0;
}

static const struct file_operations msgdma_strm_fops = {
	.owner = THIS_MODULE,
	.open = msgdma_strm_open,
	.release = msgdma_strm_release,
	.read = msgdma_strm_read,
	.write = msgdma_strm_write
};

static void msgdma_strm_setup_cdev(struct msgdma_strm *streamer, int index)
{
	int err, devno = MKDEV(streamer->major, streamer->minor + index);

	cdev_init(&streamer->cdev, &msgdma_strm_fops);
	streamer->cdev.owner = THIS_MODULE;
	streamer->cdev.ops = &msgdma_strm_fops;
	err = cdev_add(&streamer->cdev, devno, 1);
	if (err) {
		printk(KERN_NOTICE "Error %d adding msgdma_strm%d", err, index);
	}
}

static int msgdma_strm_register_chdev(struct msgdma_strm *streamer)
{
	int result;

	if (streamer->major) {
		streamer->dev = MKDEV(streamer->major, streamer->minor);
		result = register_chrdev_region(
			streamer->dev, MSGDMA_STRM_STREAMER_NR_DEVS, DEV_NAME);
	} else {
		result = alloc_chrdev_region(&streamer->dev, streamer->minor,
					     MSGDMA_STRM_STREAMER_NR_DEVS,
					     DEV_NAME);
		streamer->major = MAJOR(streamer->dev);
	}

	if (result < 0) {
		return result;
	}

	/* TODO is there any race condition with access to minor_cnt? */
	msgdma_strm_setup_cdev(streamer, msgdma_strm_device.minor_cnt++);

	return result;
}

static void msgdma_strm_unregister_chdev(struct msgdma_strm *streamer)
{
	unregister_chrdev_region(streamer->dev, MSGDMA_STRM_STREAMER_NR_DEVS);
}

static int msgdma_strm_prepare_sg(struct device *dev, int idx,
				  struct msgdma_strm_buf *cbuf,
				  enum dma_data_direction data_dir)
{
	int i_sg;
	int ret;

	for (i_sg = 0; i_sg < MSGDMA_STRM_BUF_N_PAGES; ++i_sg) {
		/* set page, length and offset ... */
		sg_set_page(&cbuf->buf[idx][i_sg], cbuf->pages[idx][i_sg],
			    MSGDMA_STRM_BUF_PAGE_SIZE, 0);
	}

	/* mark the last one as end */
	sg_mark_end(&cbuf->buf[idx][MSGDMA_STRM_BUF_N_PAGES - 1]);

	/* ... and do the mapping */
	ret = dma_map_sg(dev, &cbuf->buf[idx][0], MSGDMA_STRM_BUF_N_PAGES,
			 data_dir);
	if (ret != MSGDMA_STRM_BUF_N_PAGES) {
		goto fail;
	}
	return 0;

fail:
	/* undo mapping */
	for (i_sg = 0; i_sg < MSGDMA_STRM_BUF_N_PAGES; ++i_sg) {
		dma_unmap_sg(dev, &cbuf->buf[idx][i_sg],
			     MSGDMA_STRM_BUF_N_PAGES, data_dir);
	}
	return -ENOMEM;
}

static void msgdma_strm_unprepare_sg(struct device *dev, int idx,
				     struct msgdma_strm_buf *cbuf,
				     enum dma_data_direction data_dir)
{
	dma_unmap_sg(dev, &cbuf->buf[idx][0], MSGDMA_STRM_BUF_N_PAGES,
		     data_dir);
}

static struct msgdma_strm_buf *
msgdma_strm_init_buf(struct device *dev, enum dma_data_direction data_dir)
{
	int i_row, i_page;
	struct msgdma_strm_buf *cbuf = NULL;

	/* allocate read buffer struct */
	cbuf = (struct msgdma_strm_buf *)devm_kzalloc(
		dev, sizeof(struct msgdma_strm_buf), GFP_KERNEL);
	if (cbuf == NULL) {
		cbuf = ERR_PTR(-ENOMEM);
		goto fail1;
	}

	init_waitqueue_head(&cbuf->inq);

	/* set DMA direction */
	if (data_dir == DMA_FROM_DEVICE) {
		cbuf->trans_dir = DMA_DEV_TO_MEM;
	} else if (data_dir == DMA_TO_DEVICE) {
		cbuf->trans_dir = DMA_MEM_TO_DEV;
	} else {
		cbuf = ERR_PTR(-EINVAL);
		goto fail1;
	}

	/* allocate pages */
	for (i_row = 0; i_row < MSGDMA_STRM_BUF_N_ROWS; ++i_row) {
		for (i_page = 0; i_page < MSGDMA_STRM_BUF_N_PAGES; ++i_page) {
			cbuf->pages[i_row][i_page] =
				alloc_page(GFP_DMA | GFP_KERNEL);
			if (cbuf->pages[i_row][i_page] == NULL) {
				cbuf = ERR_PTR(-ENOMEM);
				goto fail1;
			}
		}
	}

	/* prepare sg */
	for (i_row = 0; i_row < MSGDMA_STRM_BUF_N_ROWS; ++i_row) {
		int ret = msgdma_strm_prepare_sg(dev, i_row, cbuf, data_dir);
		if (ret < 0) {
			cbuf = ERR_PTR(ret);
			goto fail2;
		}
	}

	return cbuf;

fail2:
	--i_row;
	for (; i_row >= 0; --i_row) {
		msgdma_strm_unprepare_sg(dev, i_row, cbuf, data_dir);
	}
fail1:
	return cbuf;
}

static void msgdma_strm_remove_buf(struct device *dev,
				   struct msgdma_strm_buf *cbuf,
				   enum dma_data_direction data_dir)
{
	int i_row, i_page;
	for (i_row = 0; i_row < MSGDMA_STRM_BUF_N_ROWS; ++i_row) {
		msgdma_strm_unprepare_sg(dev, i_row, cbuf, data_dir);
	}

	/* free pages from buffer */
	for (i_row = 0; i_row < MSGDMA_STRM_BUF_N_ROWS; ++i_row) {
		for (i_page = 0; i_page < MSGDMA_STRM_BUF_N_PAGES; ++i_page) {
			__free_page(cbuf->pages[i_row][i_page]);
		}
	}
}

static int msgdma_strm_probe(struct platform_device *pdev)
{
	int ret = 0;
	struct msgdma_strm *streamer;

	streamer = (struct msgdma_strm *)devm_kzalloc(
		&pdev->dev, sizeof(*streamer), GFP_KERNEL);
	if (streamer == NULL) {
		return -ENOMEM;
	}

	streamer->rd_buf = msgdma_strm_init_buf(&pdev->dev, DMA_FROM_DEVICE);
	if (IS_ERR(streamer->rd_buf)) {
		return PTR_ERR(streamer->rd_buf);
	}

	streamer->wr_buf = msgdma_strm_init_buf(&pdev->dev, DMA_TO_DEVICE);
	if (IS_ERR(streamer->wr_buf)) {
		return PTR_ERR(streamer->wr_buf);
	}

	streamer->major = MSGDMA_STRM_STREAMER_MAJOR;
	platform_set_drvdata(pdev, (void *)streamer);
	streamer->pdev = pdev;

	ret = msgdma_strm_register_chdev(streamer);
	if (ret < 0) {
		return ret;
	}
	return 0;
}

static int msgdma_strm_remove(struct platform_device *pdev)
{
	struct msgdma_strm *streamer =
		(struct msgdma_strm *)platform_get_drvdata(pdev);

	msgdma_strm_unregister_chdev(streamer);

	msgdma_strm_remove_buf(&pdev->dev, streamer->rd_buf, DMA_FROM_DEVICE);
	msgdma_strm_remove_buf(&pdev->dev, streamer->wr_buf, DMA_FROM_DEVICE);

	return 0;
}

static const struct of_device_id msgdma_strm_of_match[] = {
	{ .compatible = DEV_NAME },
	{}
};

static struct platform_driver msgdma_strm_drv = {
    .probe = msgdma_strm_probe,
    .remove = msgdma_strm_remove,
    .driver =
        {
            .name = DEV_NAME,
            .of_match_table = of_match_ptr(msgdma_strm_of_match),
        },
};

static int __init msgdma_strm_init(void)
{
	msgdma_strm_device.minor_cnt = 0;
	return platform_driver_register(&msgdma_strm_drv);
}

static void __exit msgdma_strm_exit(void)
{
	platform_driver_unregister(&msgdma_strm_drv);
	return;
}

module_init(msgdma_strm_init);
module_exit(msgdma_strm_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Christian Haettich <feddischson@gmail.com>");
MODULE_DESCRIPTION("DMA-Streaming Module based on Intel's MSGDMA core.");
MODULE_VERSION("0.0.1");
