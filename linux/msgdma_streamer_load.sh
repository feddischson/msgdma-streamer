#!/bin/sh

module="msgdma_streamer"
device="msgdma_streamer"
mode="644"

/sbin/modprobe $module $* || exit 1

rm -f /dev/${device}0

major=$(awk "\$2==\"$module\" {print \$1}" /proc/devices)

mknod /dev/${device}0 c $major 0

