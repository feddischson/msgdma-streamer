#include <gtest/gtest.h>

struct scatterlist {};

struct dma_cookie_t {};

struct wait_queue_head_t {};

enum dma_transfer_direction {};

struct dma_slave_config {};

#define MSGDMA_STRM_BUF_N_ROWS 4
#define MSGDMA_STRM_BUF_N_PAGES 5
#define MSGDMA_STRM_BUF_PAGE_SIZE 8
/*  The memory / buffer setup with the rows and pages is 
 *  like the following:
 *
 *    p
 *    a   row 0     1      2       3
 *    g  
 *    e   +------+------+------+------+
 *        |  0   | 40   | 80   | 120  |
 *    0   |      |      |      |      |
 *        |  7   | 47   | 87   | 127  |
 *        +---------------------------+
 *        |  8   | 48   | 88   | 128  |
 *    1   |      |      |      |      |
 *        | 15   | 55   | 95   | 135  |
 *        +---------------------------+
 *        | 16   | 56   | 96   | 136  |
 *    2   |      |      |      |      |
 *        | 23   | 63   | 103  | 143  |
 *        +---------------------------+
 *        | 24   | 64   | 104  | 144  |
 *    3   |      |      |      |      |
 *        | 31   | 71   | 111  | 151  |
 *        +---------------------------+
 *        | 32   | 72   | 112  | 152  |
 *    4   |      |      |      |      |
 *        | 39   | 79   | 119  | 159  |
 *        +------+------+------+------+
 */
#include "msgdma_streamer_buf.h"

class MSGDMA_Streamer_Test : public ::testing::Test {
 protected:
  msgdma_strm_buf buf;

  void SetUp() override {
    buf.head = 0;
    buf.tail = 0;
    buf.head_byte = 0;
    buf.tail_byte = 0;
  }
};


TEST_F(MSGDMA_Streamer_Test, row_size) { EXPECT_EQ(MSGDMA_STRM_BUF_ROW_SIZE, 40); }

TEST_F(MSGDMA_Streamer_Test, buf_size) { EXPECT_EQ(MSGDMA_STRM_BUF_SIZE, 160); }

////
// foreach_rd_page tests
//
TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_first_page_1) {

  size_t start_byte = 0;
  size_t end_byte = 7;

  std::array<size_t, 1> expected_s_byte = {0};
  std::array<size_t, 1> expected_s_page = {0};
  std::array<size_t, 1> expected_s_row = {0};
  std::array<size_t, 1> expected_len = {7};
  std::array<size_t, 1> expected_new_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_first_page_2) {

  size_t start_byte = 4;
  size_t end_byte = 7;

  std::array<size_t, 1> expected_s_byte = {4};
  std::array<size_t, 1> expected_s_page = {0};
  std::array<size_t, 1> expected_s_row = {0};
  std::array<size_t, 1> expected_len = {3};
  std::array<size_t, 1> expected_new_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_first_two_pages_1) {

  size_t start_byte = 0;
  size_t end_byte = 12;

  std::array<size_t, 2> expected_s_byte = {0, 0};
  std::array<size_t, 2> expected_s_page = {0, 1};
  std::array<size_t, 2> expected_s_row = {0, 0};
  std::array<size_t, 2> expected_len = {8, 4};
  std::array<size_t, 2> expected_new_row = {1, 0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_first_two_pages_2) {

  size_t start_byte = 7;
  size_t end_byte = 12;

  std::array<size_t, 2> expected_s_byte = {7, 0};
  std::array<size_t, 2> expected_s_page = {0, 1};
  std::array<size_t, 2> expected_s_row = {0, 0};
  std::array<size_t, 2> expected_len = {1, 4};
  std::array<size_t, 2> expected_new_row = {1, 0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}

//
//    p
//    a   row 0     1      2       3
//    g  
//    e   +------+------+------+------+
//        |  0   | 40   | 80   | 120  |
//    0   |      | XXX  |      |      |
//        |  7   | 47   | 87   | 127  |
//        +---------------------------+
//        |  8   | 48   | 88   | 128  |
//    1   | XXX  | XXX  |      |      |
//        | 15   | 55   | 95   | 135  |
//        +---------------------------+
//        | 16   | 56   | 96   | 136  |
//    2   | XXX  | XXX  |      |      |
//        | 23   | 63   | 103  | 143  |
//        +---------------------------+
//        | 24   | 64   | 104  | 144  |
//    3   | XXX  |      |      |      |
//        | 31   | 71   | 111  | 151  |
//        +---------------------------+
//        | 32   | 72   | 112  | 152  |
//    4   | XXX  |      |      |      |
//        | 39   | 79   | 119  | 159  |
//        +------+------+------+------+
TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_multiple_pages_1) {

  size_t start_byte = 9;
  size_t end_byte = 59;

  std::array<size_t, 7> expected_s_byte = {1, 0, 0, 0, 0, 0, 0};
  std::array<size_t, 7> expected_s_page = {1, 2, 3, 4, 0, 1, 2};
  std::array<size_t, 7> expected_s_row = {0, 0, 0, 0, 1, 1, 1};
  std::array<size_t, 7> expected_len = {7, 8, 8, 8, 8, 8, 3};
  std::array<size_t, 7> expected_new_row = {1, 0, 0, 0, 1, 0, 0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}


//
//    p
//    a   row 0     1      2       3
//    g  
//    e   +------+------+------+------+
//        |  0   | 40   | 80   | 120  |
//    0   | XXX  | XXX  |      |      |
//        |  7   | 47   | 87   | 127  |
//        +---------------------------+
//        |  8   | 48   | 88   | 128  |
//    1   | XXX  |      |      |      |
//        | 15   | 55   | 95   | 135  |
//        +---------------------------+
//        | 16   | 56   | 96   | 136  |
//    2   | XXX  |      |      |      |
//        | 23   | 63   | 103  | 143  |
//        +---------------------------+
//        | 24   | 64   | 104  | 144  |
//    3   | XXX  |      |      | XXX  |
//        | 31   | 71   | 111  | 151  |
//        +---------------------------+
//        | 32   | 72   | 112  | 152  |
//    4   | XXX  |      |      | XXX  |
//        | 39   | 79   | 119  | 159  |
//        +------+------+------+------+
TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_multiple_pages_2) {

  size_t start_byte = 151;
  size_t end_byte = 41;

  std::array<size_t, 8> expected_s_byte = {7, 0, 0, 0, 0, 0, 0, 0};
  std::array<size_t, 8> expected_s_page = {3, 4, 0, 1, 2, 3, 4, 0};
  std::array<size_t, 8> expected_s_row = {3, 3, 0, 0, 0, 0, 0, 1};
  std::array<size_t, 8> expected_len = {1, 8, 8, 8, 8, 8, 8, 1};
  std::array<size_t, 8> expected_new_row = {1, 0, 1, 0, 0, 0, 0, 1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}


TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_last_and_first_byte) {

  size_t start_byte = 159;
  size_t end_byte = 1;

  std::array<size_t, 2> expected_s_byte = {7,0};
  std::array<size_t, 2> expected_s_page = {4,0};
  std::array<size_t, 2> expected_s_row = {3, 0};
  std::array<size_t, 2> expected_len = {1,1};
  std::array<size_t, 2> expected_new_row = {1, 1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_last_page) {

  size_t start_byte = 154;
  size_t end_byte = 0;

  std::array<size_t, 1> expected_s_byte = {2};
  std::array<size_t, 1> expected_s_page = {4};
  std::array<size_t, 1> expected_s_row = {3};
  std::array<size_t, 1> expected_len = {6};
  std::array<size_t, 1> expected_new_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_last_byte_in_page) {

  size_t start_byte = 7;
  size_t end_byte = 8;

  std::array<size_t, 1> expected_s_byte = {7};
  std::array<size_t, 1> expected_s_page = {0};
  std::array<size_t, 1> expected_s_row = {0};
  std::array<size_t, 1> expected_len = {1};
  std::array<size_t, 1> expected_new_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_last_byte_in_row) {

  size_t start_byte = 39;
  size_t end_byte = 40;

  std::array<size_t, 1> expected_s_byte = {7};
  std::array<size_t, 1> expected_s_page = {4};
  std::array<size_t, 1> expected_s_row = {0};
  std::array<size_t, 1> expected_len = {1};
  std::array<size_t, 1> expected_new_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}



TEST_F(MSGDMA_Streamer_Test, foreach_rd_page_last_byte) {

  size_t start_byte = 159;
  size_t end_byte = 0;

  std::array<size_t, 1> expected_s_byte = {7};
  std::array<size_t, 1> expected_s_page = {4};
  std::array<size_t, 1> expected_s_row = {3};
  std::array<size_t, 1> expected_len = {1};
  std::array<size_t, 1> expected_new_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int new_row;
  foreach_rd_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, new_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(new_row, expected_new_row.at(idx));
    ++idx;
  }
}


////
// foreach_wr_page tests
//
TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_first_page_1) {

  size_t start_byte = 0;
  size_t end_byte = 7;

  std::array<size_t, 1> expected_s_byte = {0};
  std::array<size_t, 1> expected_s_page = {0};
  std::array<size_t, 1> expected_s_row = {0};
  std::array<size_t, 1> expected_len = {7};
  std::array<size_t, 1> expected_end_row = {0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_first_page_2) {

  size_t start_byte = 4;
  size_t end_byte = 7;

  std::array<size_t, 1> expected_s_byte = {4};
  std::array<size_t, 1> expected_s_page = {0};
  std::array<size_t, 1> expected_s_row = {0};
  std::array<size_t, 1> expected_len = {3};
  std::array<size_t, 1> expected_end_row = {0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_first_two_pages_1) {

  size_t start_byte = 0;
  size_t end_byte = 12;

  std::array<size_t, 2> expected_s_byte = {0, 0};
  std::array<size_t, 2> expected_s_page = {0, 1};
  std::array<size_t, 2> expected_s_row = {0, 0};
  std::array<size_t, 2> expected_len = {8, 4};
  std::array<size_t, 2> expected_end_row = {0, 0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_first_two_pages_2) {

  size_t start_byte = 7;
  size_t end_byte = 12;

  std::array<size_t, 2> expected_s_byte = {7, 0};
  std::array<size_t, 2> expected_s_page = {0, 1};
  std::array<size_t, 2> expected_s_row = {0, 0};
  std::array<size_t, 2> expected_len = {1, 4};
  std::array<size_t, 2> expected_end_row = {0, 0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}

//
//    p
//    a   row 0     1      2       3
//    g  
//    e   +------+------+------+------+
//        |  0   | 40   | 80   | 120  |
//    0   |      | XXX  |      |      |
//        |  7   | 47   | 87   | 127  |
//        +---------------------------+
//        |  8   | 48   | 88   | 128  |
//    1   | XXX  | XXX  |      |      |
//        | 15   | 55   | 95   | 135  |
//        +---------------------------+
//        | 16   | 56   | 96   | 136  |
//    2   | XXX  | XXX  |      |      |
//        | 23   | 63   | 103  | 143  |
//        +---------------------------+
//        | 24   | 64   | 104  | 144  |
//    3   | XXX  |      |      |      |
//        | 31   | 71   | 111  | 151  |
//        +---------------------------+
//        | 32   | 72   | 112  | 152  |
//    4   | XXX  |      |      |      |
//        | 39   | 79   | 119  | 159  |
//        +------+------+------+------+
TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_multiple_pages_1) {

  size_t start_byte = 9;
  size_t end_byte = 59;

  std::array<size_t, 7> expected_s_byte =  {1, 0, 0, 0, 0, 0, 0};
  std::array<size_t, 7> expected_s_page =  {1, 2, 3, 4, 0, 1, 2};
  std::array<size_t, 7> expected_s_row =   {0, 0, 0, 0, 1, 1, 1};
  std::array<size_t, 7> expected_len =     {7, 8, 8, 8, 8, 8, 3};
  std::array<size_t, 7> expected_end_row = {0, 0, 0, 1, 0, 0, 0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}


//
//    p
//    a   row 0     1      2       3
//    g  
//    e   +------+------+------+------+
//        |  0   | 40   | 80   | 120  |
//    0   | XXX  | XXX  |      |      |
//        |  7   | 47   | 87   | 127  |
//        +---------------------------+
//        |  8   | 48   | 88   | 128  |
//    1   | XXX  |      |      |      |
//        | 15   | 55   | 95   | 135  |
//        +---------------------------+
//        | 16   | 56   | 96   | 136  |
//    2   | XXX  |      |      |      |
//        | 23   | 63   | 103  | 143  |
//        +---------------------------+
//        | 24   | 64   | 104  | 144  |
//    3   | XXX  |      |      | XXX  |
//        | 31   | 71   | 111  | 151  |
//        +---------------------------+
//        | 32   | 72   | 112  | 152  |
//    4   | XXX  |      |      | XXX  |
//        | 39   | 79   | 119  | 159  |
//        +------+------+------+------+
TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_multiple_pages_2) {

  size_t start_byte = 151;
  size_t end_byte = 41;

  std::array<size_t, 8> expected_s_byte = {7, 0, 0, 0, 0, 0, 0, 0};
  std::array<size_t, 8> expected_s_page = {3, 4, 0, 1, 2, 3, 4, 0};
  std::array<size_t, 8> expected_s_row = {3, 3, 0, 0, 0, 0, 0, 1};
  std::array<size_t, 8> expected_len = {1, 8, 8, 8, 8, 8, 8, 1};
  std::array<size_t, 8> expected_end_row = {0, 1, 0, 0, 0, 0, 1, 0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}


TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_last_and_first_byte) {

  size_t start_byte = 159;
  size_t end_byte = 1;

  std::array<size_t, 2> expected_s_byte = {7,0};
  std::array<size_t, 2> expected_s_page = {4,0};
  std::array<size_t, 2> expected_s_row = {3, 0};
  std::array<size_t, 2> expected_len = {1,1};
  std::array<size_t, 2> expected_end_row = {1, 0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_last_page) {

  size_t start_byte = 154;
  size_t end_byte = 0;

  std::array<size_t, 1> expected_s_byte = {2};
  std::array<size_t, 1> expected_s_page = {4};
  std::array<size_t, 1> expected_s_row = {3};
  std::array<size_t, 1> expected_len = {6};
  std::array<size_t, 1> expected_end_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_last_byte_in_page) {

  size_t start_byte = 7;
  size_t end_byte = 8;

  std::array<size_t, 1> expected_s_byte = {7};
  std::array<size_t, 1> expected_s_page = {0};
  std::array<size_t, 1> expected_s_row = {0};
  std::array<size_t, 1> expected_len = {1};
  std::array<size_t, 1> expected_end_row = {0};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}

TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_last_byte_in_row) {

  size_t start_byte = 39;
  size_t end_byte = 40;

  std::array<size_t, 1> expected_s_byte = {7};
  std::array<size_t, 1> expected_s_page = {4};
  std::array<size_t, 1> expected_s_row = {0};
  std::array<size_t, 1> expected_len = {1};
  std::array<size_t, 1> expected_end_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}



TEST_F(MSGDMA_Streamer_Test, foreach_wr_page_last_byte) {

  size_t start_byte = 159;
  size_t end_byte = 0;

  std::array<size_t, 1> expected_s_byte = {7};
  std::array<size_t, 1> expected_s_page = {4};
  std::array<size_t, 1> expected_s_row = {3};
  std::array<size_t, 1> expected_len = {1};
  std::array<size_t, 1> expected_end_row = {1};

  size_t s_byte, e_byte;
  size_t s_page, e_page;
  size_t s_row, e_row;
  size_t len;
  size_t idx = 0;
  int end_row;
  foreach_wr_page(start_byte, end_byte, s_byte, s_page, s_row, e_byte, e_page,
                   e_row, len, end_row) {
    EXPECT_EQ(s_byte, expected_s_byte.at(idx));
    EXPECT_EQ(s_page, expected_s_page.at(idx));
    EXPECT_EQ(s_row, expected_s_row.at(idx));
    EXPECT_EQ(len, expected_len.at(idx));
    EXPECT_EQ(end_row, expected_end_row.at(idx));
    ++idx;
  }
}






TEST_F(MSGDMA_Streamer_Test, move_head) {
  dma_buf_move_head(&buf);
  EXPECT_EQ(buf.head, 1);
  EXPECT_EQ(buf.head_byte, 1 * MSGDMA_STRM_BUF_ROW_SIZE);

  dma_buf_move_head(&buf);
  EXPECT_EQ(buf.head, 2);
  EXPECT_EQ(buf.head_byte, 2 * MSGDMA_STRM_BUF_ROW_SIZE);

  dma_buf_move_head(&buf);
  EXPECT_EQ(buf.head, 3);
  EXPECT_EQ(buf.head_byte, 3 * MSGDMA_STRM_BUF_ROW_SIZE);

  dma_buf_move_head(&buf);
  EXPECT_EQ(buf.head, 0);
  EXPECT_EQ(buf.head_byte, 0 * MSGDMA_STRM_BUF_ROW_SIZE);
}

TEST_F(MSGDMA_Streamer_Test, idx_calculation_1) {
  size_t byte;
  size_t byte_idx, page_idx, row_idx;

  // byte = 2 => 2nd byte in page 0 and row 0
  byte = 2;
  dma_buf_calc_idx(byte, &byte_idx, &page_idx, &row_idx);
  EXPECT_EQ(byte_idx, 2);
  EXPECT_EQ(page_idx, 0);
  EXPECT_EQ(row_idx, 0);
}

TEST_F(MSGDMA_Streamer_Test, idx_calculation_2) {
  size_t byte;
  size_t byte_idx, page_idx, row_idx;

  // byte = 7 => 7th byte in page 0 and row 0
  byte = 7;
  dma_buf_calc_idx(byte, &byte_idx, &page_idx, &row_idx);
  EXPECT_EQ(byte_idx, 7);
  EXPECT_EQ(page_idx, 0);
  EXPECT_EQ(row_idx, 0);
}

TEST_F(MSGDMA_Streamer_Test, idx_calculation_3) {
  size_t byte;
  size_t byte_idx, page_idx, row_idx;

  // byte = 8 => 1th byte in page 1 and row 0
  byte = 8;
  dma_buf_calc_idx(byte, &byte_idx, &page_idx, &row_idx);
  EXPECT_EQ(byte_idx, 0);
  EXPECT_EQ(page_idx, 1);
  EXPECT_EQ(row_idx, 0);
}

TEST_F(MSGDMA_Streamer_Test, idx_calculation_4) {
  size_t byte;
  size_t byte_idx, page_idx, row_idx;

  // byte = 39 => 7th byte in page 4 and row 0
  byte = 39;
  dma_buf_calc_idx(byte, &byte_idx, &page_idx, &row_idx);
  EXPECT_EQ(byte_idx, 7);
  EXPECT_EQ(page_idx, 4);
  EXPECT_EQ(row_idx, 0);
}

TEST_F(MSGDMA_Streamer_Test, idx_calculation_5) {
  size_t byte;
  size_t byte_idx, page_idx, row_idx;

  // byte = 40 => 0th byte in page 0 and row 1
  byte = 40;
  dma_buf_calc_idx(byte, &byte_idx, &page_idx, &row_idx);
  EXPECT_EQ(byte_idx, 0);
  EXPECT_EQ(page_idx, 0);
  EXPECT_EQ(row_idx, 1);
}

TEST_F(MSGDMA_Streamer_Test, idx_calculation_6) {
  size_t byte;
  size_t byte_idx, page_idx, row_idx;

  // byte = 50 => 3th byte in page 1 and row 1
  byte = 50;
  dma_buf_calc_idx(byte, &byte_idx, &page_idx, &row_idx);
  EXPECT_EQ(byte_idx, 2);
  EXPECT_EQ(page_idx, 1);
  EXPECT_EQ(row_idx, 1);
}

TEST_F(MSGDMA_Streamer_Test, idx_calculation_7) {
  size_t byte;
  size_t byte_idx, page_idx, row_idx;

  // byte = 159 => last byte in last row in last page
  byte = MSGDMA_STRM_BUF_SIZE - 1;
  dma_buf_calc_idx(byte, &byte_idx, &page_idx, &row_idx);
  EXPECT_EQ(byte_idx, 7);
  EXPECT_EQ(page_idx, 4);
  EXPECT_EQ(row_idx, 3);
}

TEST_F(MSGDMA_Streamer_Test, idx_calculation_8) {
  size_t byte;
  size_t byte_idx, page_idx, row_idx;

  // byte = 160 => first byte in first row in first page
  byte = MSGDMA_STRM_BUF_SIZE;
  dma_buf_calc_idx(byte, &byte_idx, &page_idx, &row_idx);
  EXPECT_EQ(byte_idx, 0);
  EXPECT_EQ(page_idx, 0);
  EXPECT_EQ(row_idx, 0);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
