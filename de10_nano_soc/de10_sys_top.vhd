--
-- TODO
-- Copyright (C) 2020 Christian Haettich [feedischson@gmail.com].
-- All rights reserved.
--


--! IEEE standard library
library ieee;
--! IEEE std_logic_1164 multi-value logic system
use ieee.std_logic_1164.all;
--! Standard VHDL Synthesis Packages (IEEE Std 1076.3, NUMERIC_STD)
use ieee.numeric_std.all;

entity de10_sys_top is
  port(

   -- ADC
   adc_convst         :   out std_logic;
   adc_sck            :   out std_logic;
   adc_sdi            :   out std_logic;
   adc_sdo            :    in std_logic;

   -- CLOCK
   fpga_clk1_50       :    in std_logic;
   fpga_clk2_50       :    in std_logic;
   fpga_clk3_50       :    in std_logic;

   -- HPS
   hps_conv_usb_n     : inout std_logic;
   hps_ddr3_addr      :   out std_logic_vector( 15-1 downto 0 );
   hps_ddr3_ba        :   out std_logic_vector( 3-1 downto 0  );
   hps_ddr3_cas_n     :   out std_logic;
   hps_ddr3_ck_n      :   out std_logic;
   hps_ddr3_ck_p      :   out std_logic;
   hps_ddr3_cke       :   out std_logic;
   hps_ddr3_cs_n      :   out std_logic;
   hps_ddr3_dm        :   out std_logic_vector( 4-1 downto 0  );
   hps_ddr3_dq        : inout std_logic_vector( 32-1 downto 0  );
   hps_ddr3_dqs_n     : inout std_logic_vector( 4-1 downto 0  );
   hps_ddr3_dqs_p     : inout std_logic_vector( 4-1 downto 0  );
   hps_ddr3_odt       :   out std_logic;
   hps_ddr3_ras_n     :   out std_logic;
   hps_ddr3_reset_n   :   out std_logic;
   hps_ddr3_rzq       :    in std_logic;
   hps_ddr3_we_n      :   out std_logic;
   hps_enet_gtx_clk   :   out std_logic;
   hps_enet_int_n     : inout std_logic;
   hps_enet_mdc       :   out std_logic;
   hps_enet_mdio      : inout std_logic;
   hps_enet_rx_clk    :    in std_logic;
   hps_enet_rx_data   :    in std_logic_vector( 4-1 downto 0  );
   hps_enet_rx_dv     :    in std_logic;
   hps_enet_tx_data   :   out std_logic_vector( 4-1 downto 0  );
   hps_enet_tx_en     :   out std_logic;
   hps_gsensor_int    : inout std_logic;
   hps_i2c0_sclk      : inout std_logic;
   hps_i2c0_sdat      : inout std_logic;
   hps_i2c1_sclk      : inout std_logic;
   hps_i2c1_sdat      : inout std_logic;
   hps_key            : inout std_logic;
   hps_led            : inout std_logic;
   hps_ltc_gpio       : inout std_logic;
   hps_sd_clk         :   out std_logic;
   hps_sd_cmd         : inout std_logic;
   hps_sd_data        : inout std_logic_vector( 4-1 downto 0  );
   hps_spim_clk       :   out std_logic;
   hps_spim_miso      :    in std_logic;
   hps_spim_mosi      :   out std_logic;
   hps_spim_ss        : inout std_logic;
   hps_uart_rx        :    in std_logic;
   hps_uart_tx        :   out std_logic;
   hps_usb_clkout     :    in std_logic;
   hps_usb_data       : inout std_logic_vector( 8-1 downto 0  );
   hps_usb_dir        :    in std_logic;
   hps_usb_nxt        :    in std_logic;
   hps_usb_stp        :   out std_logic;

   -- KEY
   key                :    in std_logic_vector( 2-1 downto 0 );

   -- LED
   led                :   out std_logic_vector( 8-1 downto 0 );

   -- SWITCHES
   sw                 :    in std_logic_vector( 4-1 downto 0 )
);

end entity de10_sys_top;

architecture imp of de10_sys_top is


  -- internal reset
  signal reset                : std_logic;
  signal h2f_reset            : std_logic;

  signal warm_reset_hs_ack    : std_logic;
  signal warm_reset_hs_req_n  : std_logic;
  signal pio_ctrl             : std_logic_vector( 8-1 downto 0 );
  signal pio_reset_detect     : std_logic;

  signal max2769_idle         : std_logic;
  signal max2769_shdn         : std_logic;

  signal spi_miso             : std_logic;
  signal spi_mosi             : std_logic;
  signal spi_sclk             : std_logic;
  signal spi_ss_n             : std_logic;

  signal data_valid           : std_logic;
  signal led_hps              : std_logic_vector(4-1 downto 0 );

  signal dma_to_stream_startofpacket  : std_logic;
  signal dma_to_stream_endofpacket    : std_logic;
  signal dma_to_stream_empty          : std_logic_vector(1 downto 0);

  component de10_sys is
  port (
    clk_clk                          : in    std_logic;
    dma_to_stream_data               : out   std_logic_vector(31 downto 0);
    dma_to_stream_valid              : out   std_logic;
    dma_to_stream_ready              : in    std_logic;
    dma_to_stream_startofpacket      : out   std_logic;
    dma_to_stream_endofpacket        : out   std_logic;
    dma_to_stream_empty              : out   std_logic_vector(1 downto 0);
    h2f_reset_reset_n                : out   std_logic;
    hps_io_hps_io_emac1_inst_TX_CLK  : out   std_logic;
    hps_io_hps_io_emac1_inst_TXD0    : out   std_logic;
    hps_io_hps_io_emac1_inst_TXD1    : out   std_logic;
    hps_io_hps_io_emac1_inst_TXD2    : out   std_logic;
    hps_io_hps_io_emac1_inst_TXD3    : out   std_logic;
    hps_io_hps_io_emac1_inst_RXD0    : in    std_logic;
    hps_io_hps_io_emac1_inst_MDIO    : inout std_logic;
    hps_io_hps_io_emac1_inst_MDC     : out   std_logic;
    hps_io_hps_io_emac1_inst_RX_CTL  : in    std_logic;
    hps_io_hps_io_emac1_inst_TX_CTL  : out   std_logic;
    hps_io_hps_io_emac1_inst_RX_CLK  : in    std_logic;
    hps_io_hps_io_emac1_inst_RXD1    : in    std_logic;
    hps_io_hps_io_emac1_inst_RXD2    : in    std_logic;
    hps_io_hps_io_emac1_inst_RXD3    : in    std_logic;
    hps_io_hps_io_sdio_inst_CMD      : inout std_logic;
    hps_io_hps_io_sdio_inst_D0       : inout std_logic;
    hps_io_hps_io_sdio_inst_D1       : inout std_logic;
    hps_io_hps_io_sdio_inst_CLK      : out   std_logic;
    hps_io_hps_io_sdio_inst_D2       : inout std_logic;
    hps_io_hps_io_sdio_inst_D3       : inout std_logic;
    hps_io_hps_io_usb1_inst_D0       : inout std_logic;
    hps_io_hps_io_usb1_inst_D1       : inout std_logic;
    hps_io_hps_io_usb1_inst_D2       : inout std_logic;
    hps_io_hps_io_usb1_inst_D3       : inout std_logic;
    hps_io_hps_io_usb1_inst_D4       : inout std_logic;
    hps_io_hps_io_usb1_inst_D5       : inout std_logic;
    hps_io_hps_io_usb1_inst_D6       : inout std_logic;
    hps_io_hps_io_usb1_inst_D7       : inout std_logic;
    hps_io_hps_io_usb1_inst_CLK      : in    std_logic;
    hps_io_hps_io_usb1_inst_STP      : out   std_logic;
    hps_io_hps_io_usb1_inst_DIR      : in    std_logic;
    hps_io_hps_io_usb1_inst_NXT      : in    std_logic;
    hps_io_hps_io_spim1_inst_CLK     : out   std_logic;
    hps_io_hps_io_spim1_inst_MOSI    : out   std_logic;
    hps_io_hps_io_spim1_inst_MISO    : in    std_logic;
    hps_io_hps_io_spim1_inst_SS0     : out   std_logic;
    hps_io_hps_io_uart0_inst_RX      : in    std_logic;
    hps_io_hps_io_uart0_inst_TX      : out   std_logic;
    hps_io_hps_io_i2c0_inst_SDA      : inout std_logic;
    hps_io_hps_io_i2c0_inst_SCL      : inout std_logic;
    hps_io_hps_io_i2c1_inst_SDA      : inout std_logic;
    hps_io_hps_io_i2c1_inst_SCL      : inout std_logic;
    hps_io_hps_io_gpio_inst_GPIO09   : inout std_logic;
    hps_io_hps_io_gpio_inst_GPIO35   : inout std_logic;
    hps_io_hps_io_gpio_inst_GPIO40   : inout std_logic;
    hps_io_hps_io_gpio_inst_GPIO53   : inout std_logic;
    hps_io_hps_io_gpio_inst_GPIO54   : inout std_logic;
    hps_io_hps_io_gpio_inst_GPIO61   : inout std_logic;
    led_out_export                   : out   std_logic_vector(4-1 downto 0);
    memory_mem_a                     : out   std_logic_vector(14 downto 0);
    memory_mem_ba                    : out   std_logic_vector(2 downto 0);
    memory_mem_ck                    : out   std_logic;
    memory_mem_ck_n                  : out   std_logic;
    memory_mem_cke                   : out   std_logic;
    memory_mem_cs_n                  : out   std_logic;
    memory_mem_ras_n                 : out   std_logic;
    memory_mem_cas_n                 : out   std_logic;
    memory_mem_we_n                  : out   std_logic;
    memory_mem_reset_n               : out   std_logic;
    memory_mem_dq                    : inout std_logic_vector(31 downto 0);
    memory_mem_dqs                   : inout std_logic_vector(3 downto 0);
    memory_mem_dqs_n                 : inout std_logic_vector(3 downto 0);
    memory_mem_odt                   : out   std_logic;
    memory_mem_dm                    : out   std_logic_vector(3 downto 0);
    memory_oct_rzqin                 : in    std_logic;
    pb_in_export                     : in    std_logic_vector(1 downto 0);
    pio_ctrl_export                  : out   std_logic_vector(7 downto 0);
    reset_reset_n                    : in    std_logic;
    stream_to_dma_data               : in    std_logic_vector(31 downto 0);
    stream_to_dma_valid              : in    std_logic;
    stream_to_dma_ready              : out   std_logic;
    sw_in_export                     : in    std_logic_vector(3 downto 0);
    warm_reset_handshake_h2f_pending_rst_req_n : out   std_logic;
    warm_reset_handshake_f2h_pending_rst_ack_n : in    std_logic
  );
  end component de10_sys;

  signal hps_warm_reset : std_logic;

  signal data_tmp1  : std_logic_vector( 4-1 downto 0 );
  signal data_tmp2  : std_logic_vector( 4-1 downto 0 );

  signal dma_to_stream_data          : std_logic_vector(32-1 downto 0);
  signal dma_to_stream_valid         : std_logic;
  signal dma_to_stream_ready         : std_logic;


  signal stream_to_dma_data          : std_logic_vector(32-1 downto 0);
  signal stream_to_dma_valid         : std_logic;
  signal stream_to_dma_ready         : std_logic;


  type TEST_IN_OUT is record

      out_cnt        : unsigned(32-1 downto 0);
      dly_cnt        : unsigned( 8-1 downto 0);

      in_cnt         : unsigned(32-1 downto 0);

      in_first       : std_logic;
      in_err         : std_logic;
      in_err_word    : std_logic_vector(32-1 downto 0 );

  end record TEST_IN_OUT;

  signal st : TEST_IN_OUT;


begin

--=======================================================
--  Structural coding
--=======================================================

STREAM_P : process( FPGA_CLK1_50 )
  begin
  if FPGA_CLK1_50'event and FPGA_CLK1_50='1' then
    if pio_ctrl(7) = '1' or reset = '1' then
       st <= ( out_cnt => (others=>'0'),
               dly_cnt => (others => '0'),
               in_cnt => (others => '0'),
               in_first => '0',
               in_err   => '0',
               in_err_word => (others => '0'));
    else
       st <= st;

       if pio_ctrl(0) = '1' then
          if pio_ctrl(3 downto 1) = "000" and st.dly_cnt = to_unsigned(1, st.dly_cnt'length) then
             st.dly_cnt <= (others => '0');

          elsif pio_ctrl(3 downto 1) = "001" and st.dly_cnt = to_unsigned(2, st.dly_cnt'length) then
             st.dly_cnt <= (others => '0');

          elsif pio_ctrl(3 downto 1) = "010" and st.dly_cnt = to_unsigned(4, st.dly_cnt'length) then
             st.dly_cnt <= (others => '0');

          elsif pio_ctrl(3 downto 1) = "011" and st.dly_cnt = to_unsigned(8, st.dly_cnt'length) then
             st.dly_cnt <= (others => '0');

          elsif pio_ctrl(3 downto 1) = "100" and st.dly_cnt = to_unsigned(16, st.dly_cnt'length) then
             st.dly_cnt <= (others => '0');

          elsif pio_ctrl(3 downto 1) = "101" and st.dly_cnt = to_unsigned(3, st.dly_cnt'length2) then
             st.dly_cnt <= (others => '0');

          elsif pio_ctrl(3 downto 1) = "110" and st.dly_cnt = to_unsigned(64, st.dly_cnt'length) then
             st.dly_cnt <= (others => '0');

          elsif pio_ctrl(3 downto 1) = "111" and st.dly_cnt = to_unsigned(128, st.dly_cnt'length) then
             st.dly_cnt <= (others => '0');
          else
             st.dly_cnt <= st.dly_cnt +1;
          end if;

          if st.dly_cnt = to_unsigned(0, st.dly_cnt'length) then
            st.out_cnt <= st.out_cnt + 1;
          end if;

          if st.in_first = '0' then
            if dma_to_stream_valid = '1' then
               st.in_first <= '1';
               st.in_cnt <= unsigned(dma_to_stream_data);
            end if;
          else
            if st.dly_cnt = to_unsigned(0, st.dly_cnt'length) then
            end if;
          end if;

       else
          if stream_to_dma_valid = '1' then
             st.out_cnt <= st.out_cnt + 1;
          end if;
       end if;


    end if;
  end if;
  end process STREAM_P;

  stream_to_dma_data  <= std_logic_vector(st.out_cnt);
  stream_to_dma_valid <= stream_to_dma_ready and pio_ctrl(6);
  dma_to_stream_ready <= dma_to_stream_valid and pio_ctrl(6);


-- the ADCs are not used at the moment
adc_convst  <= '1';
adc_sck     <= '1';
adc_sdi     <= '1';

warm_reset_hs_ack <= not warm_reset_hs_req_n;
hps_warm_reset    <= not warm_reset_hs_req_n;

-- Use two of the switches to put the frontend in IDLE or shutdown mode.
led(0)             <= pio_ctrl(0);
led(1)             <= pio_ctrl(1);
led(2)             <= pio_ctrl(2);
led(3)             <= dma_to_stream_valid or
                      dma_to_stream_data(0) or
                      dma_to_stream_data(1) or
                      dma_to_stream_data(2) or
                      dma_to_stream_data(3) or
                      dma_to_stream_data(4) or
                      dma_to_stream_data(5) or
                      dma_to_stream_data(6) or
                      dma_to_stream_data(7) or
                      dma_to_stream_data(8) or
                      dma_to_stream_data(9) or
                      dma_to_stream_data(10) or
                      dma_to_stream_data(11) or
                      dma_to_stream_data(12) or
                      dma_to_stream_data(13) or
                      dma_to_stream_data(13) or
                      dma_to_stream_data(14) or
                      dma_to_stream_data(15) or
                      dma_to_stream_data(16) or
                      dma_to_stream_data(17) or
                      dma_to_stream_data(18) or
                      dma_to_stream_data(19) or
                      dma_to_stream_data(20) or
                      dma_to_stream_data(21) or
                      dma_to_stream_data(22) or
                      dma_to_stream_data(23) or
                      dma_to_stream_data(23) or
                      dma_to_stream_data(24) or
                      dma_to_stream_data(25) or
                      dma_to_stream_data(26) or
                      dma_to_stream_data(27) or
                      dma_to_stream_data(28) or
                      dma_to_stream_data(29) or
                      dma_to_stream_data(30) or
                      dma_to_stream_data(31);

led(8-1 downto 4)  <= led_hps;

-- A warm-reset on the hps-side shall also reset the FPGA part
reset <= hps_warm_reset;

top_inst : de10_sys
    port map (
        clk_clk                                    => fpga_clk1_50,
        dma_to_stream_data                         => dma_to_stream_data,
        dma_to_stream_valid                        => dma_to_stream_valid,
        dma_to_stream_ready                        => dma_to_stream_ready,
        dma_to_stream_startofpacket                => dma_to_stream_startofpacket, 
        dma_to_stream_endofpacket                  => dma_to_stream_endofpacket, 
        dma_to_stream_empty                        => dma_to_stream_empty,        
        h2f_reset_reset_n                          => h2f_reset,

        hps_io_hps_io_emac1_inst_TX_CLK            => hps_enet_gtx_clk,
        hps_io_hps_io_emac1_inst_TXD0              => hps_enet_tx_data(0),
        hps_io_hps_io_emac1_inst_TXD1              => hps_enet_tx_data(1),
        hps_io_hps_io_emac1_inst_TXD2              => hps_enet_tx_data(2),
        hps_io_hps_io_emac1_inst_TXD3              => hps_enet_tx_data(3),
        hps_io_hps_io_emac1_inst_RXD0              => hps_enet_rx_data(0),
        hps_io_hps_io_emac1_inst_RXD1              => hps_enet_rx_data(1),
        hps_io_hps_io_emac1_inst_RXD2              => hps_enet_rx_data(2),
        hps_io_hps_io_emac1_inst_RXD3              => hps_enet_rx_data(3),
        hps_io_hps_io_emac1_inst_MDIO              => hps_enet_mdio,
        hps_io_hps_io_emac1_inst_MDC               => hps_enet_mdc,
        hps_io_hps_io_emac1_inst_RX_CTL            => hps_enet_rx_dv,
        hps_io_hps_io_emac1_inst_TX_CTL            => hps_enet_tx_en,
        hps_io_hps_io_emac1_inst_RX_CLK            => hps_enet_rx_clk,

        hps_io_hps_io_sdio_inst_CLK                => hps_sd_clk,
        hps_io_hps_io_sdio_inst_CMD                => hps_sd_cmd,
        hps_io_hps_io_sdio_inst_D0                 => hps_sd_data(0),
        hps_io_hps_io_sdio_inst_D1                 => hps_sd_data(1),
        hps_io_hps_io_sdio_inst_D2                 => hps_sd_data(2),
        hps_io_hps_io_sdio_inst_D3                 => hps_sd_data(3),

        hps_io_hps_io_usb1_inst_D0                 => hps_usb_data(0),
        hps_io_hps_io_usb1_inst_D1                 => hps_usb_data(1),
        hps_io_hps_io_usb1_inst_D2                 => hps_usb_data(2),
        hps_io_hps_io_usb1_inst_D3                 => hps_usb_data(3),
        hps_io_hps_io_usb1_inst_D4                 => hps_usb_data(4),
        hps_io_hps_io_usb1_inst_D5                 => hps_usb_data(5),
        hps_io_hps_io_usb1_inst_D6                 => hps_usb_data(6),
        hps_io_hps_io_usb1_inst_D7                 => hps_usb_data(7),
        hps_io_hps_io_usb1_inst_CLK                => hps_usb_clkout,
        hps_io_hps_io_usb1_inst_STP                => hps_usb_stp,
        hps_io_hps_io_usb1_inst_DIR                => hps_usb_dir,
        hps_io_hps_io_usb1_inst_NXT                => hps_usb_nxt,

        hps_io_hps_io_spim1_inst_CLK               => hps_spim_clk,
        hps_io_hps_io_spim1_inst_MOSI              => hps_spim_mosi,
        hps_io_hps_io_spim1_inst_MISO              => hps_spim_miso,
        hps_io_hps_io_spim1_inst_SS0               => hps_spim_ss,

        hps_io_hps_io_uart0_inst_RX                => hps_uart_rx,
        hps_io_hps_io_uart0_inst_TX                => hps_uart_tx,

        hps_io_hps_io_i2c0_inst_SDA                => hps_i2c0_sdat,
        hps_io_hps_io_i2c0_inst_SCL                => hps_i2c0_sclk,
        hps_io_hps_io_i2c1_inst_SDA                => hps_i2c1_sdat,
        hps_io_hps_io_i2c1_inst_SCL                => hps_i2c1_sclk,

        hps_io_hps_io_gpio_inst_GPIO09             => hps_conv_usb_n,
        hps_io_hps_io_gpio_inst_GPIO35             => hps_enet_int_n,
        hps_io_hps_io_gpio_inst_GPIO40             => hps_ltc_gpio,
        hps_io_hps_io_gpio_inst_GPIO53             => hps_led,
        hps_io_hps_io_gpio_inst_GPIO54             => hps_key,
        hps_io_hps_io_gpio_inst_GPIO61             => hps_gsensor_int,

        led_out_export                             => led_hps,

        memory_mem_a                               => hps_ddr3_addr,
        memory_mem_ba                              => hps_ddr3_ba,
        memory_mem_ck                              => hps_ddr3_ck_p,
        memory_mem_ck_n                            => hps_ddr3_ck_n,
        memory_mem_cke                             => hps_ddr3_cke,
        memory_mem_cs_n                            => hps_ddr3_cs_n,
        memory_mem_ras_n                           => hps_ddr3_ras_n,
        memory_mem_cas_n                           => hps_ddr3_cas_n,
        memory_mem_we_n                            => hps_ddr3_we_n,
        memory_mem_reset_n                         => hps_ddr3_reset_n,
        memory_mem_dq                              => hps_ddr3_dq,
        memory_mem_dqs                             => hps_ddr3_dqs_p,
        memory_mem_dqs_n                           => hps_ddr3_dqs_n,
        memory_mem_odt                             => hps_ddr3_odt,
        memory_mem_dm                              => hps_ddr3_dm,
        memory_oct_rzqin                           => hps_ddr3_rzq,
        pb_in_export                               => key,
        pio_ctrl_export                            => pio_ctrl,
        reset_reset_n                              => not reset,

        stream_to_dma_data                         => stream_to_dma_data,
        stream_to_dma_valid                        => stream_to_dma_valid,
        stream_to_dma_ready                        => stream_to_dma_ready,

        sw_in_export                               => sw,

        warm_reset_handshake_h2f_pending_rst_req_n => warm_reset_hs_req_n,
        warm_reset_handshake_f2h_pending_rst_ack_n => not warm_reset_hs_ack
    );

end architecture imp;
