#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <byteswap.h>


int main(int argc, char *argv[]) {

  FILE * fp;
  size_t cnt = 0;

  int skip_errors = 0;



  if (argc < 2) {
    printf("Invalid usage!\n");
    printf("   dma_cont_test <file> <skip>\n");
  }

  if (argc > 2){
    skip_errors = atoi(argv[2]);
    printf("Set skip-errors to %d\n", skip_errors);
  }

  fp = fopen(argv[1], "r");

  uint32_t old_dst = 0;
  while(1) {
    uint32_t dst;
    int res;
    res = fread(&dst, sizeof(dst), 1, fp);
    if(res != 1) {
      printf("Stopping @ %lx\n", cnt*sizeof(uint32_t));
      break;
    }
    dst = bswap_32(dst);


    if(cnt > 0){
      if((old_dst+1) != dst ) {
        printf("Invalid increment @ %lx: 0x%x vs 0x%x\n", cnt*sizeof(uint32_t), old_dst, dst);
        if(skip_errors == 0 ){
          break;
        } else  {
          --skip_errors;
        }

      }
    }
    old_dst = dst;
    cnt++;
  }
  fclose(fp);
  return 0;
}
