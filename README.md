
MSGDMA-Streamer
================

The MSGDMA-Streamer should allow an easy file to data-stream and data-stream to file transfer
with hard real-time requirements (where the stream can not be paused).
The file handling is done with an embedded Linux OS and the data-stream is processed within
an FPGA. For this reason, an efficient transfer from the FPGA to the Linux OS is required.
Intel (former Altera) offers the MSGDMA IP cores, which allow an efficient stream to memory and
memory to stream DMA transfer.


This repository holds mainly two parts:
 - `./altera/qsys/msgdma_streamer.qsys`: A combination of two Intel's MSGDMA IP-Cores,
   one configured for Memory-to-Stream and one for Stream-to-Memory transfer
 - `./linux`:  A Linux kernel driver, which uses the altera-msgdma DMA driver

In addition, there is an example system (`de10_nano_soc`) for evaluation, based on the Terasic DE10-SOC board. 
The example system can then be used e.g. with the Angstrom Linux distribution.

This code is very experimental and not complete!

Howto get the stuff working
===========================


 - Patch the linux kernel (altera-msgdma.c) (see `./patches`)
 - Integrate msgdma_streamer.qsys
 - Adapt device-tree (see `./de10_nano_soc/dts/`)
 - Get Linux (e.g. Angstromg) running
 - Build kernel module
 - In case of the example, reset and enable the input/output logic 
   (see `scripts/in_out_reset.sh`, might need adaption)
 - Load the kernel module (see `linux/msgdma_streamer_load.sh`)

Open Issues  and TODOs
=======================
 - Writing is not possible due to the following reason:
   The altera-msgdma driver expects a response port for a correct handling of the descriptors.
   For some unknown reason, this response port is not available for DMA-to-Stream configuration.
   It would need a bigger change in the altera-msgdma driver to support correct DMA callback handling.

 - It would be nice to have some logic around the stream handling to detect overruns,
   to reset everything and so on.

 - It would also be nice to have a common reset interface, which resets the
   msgdma_streamer module, the altera-msgdma module and also the FPGA part.

