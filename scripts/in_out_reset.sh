#!/bin/sh


echo 1952 > /sys/class/gpio/export
echo 1951 > /sys/class/gpio/export

echo out > /sys/class/gpio/gpio1951/direction
echo out > /sys/class/gpio/gpio1952/direction

echo 1 > /sys/class/gpio/gpio1952/value
echo 1 > /sys/class/gpio/gpio1951/value
echo 0 > /sys/class/gpio/gpio1952/value
